#include <iostream>

#include "args/args.h"

namespace {

    //! class to add inputs
    class adder {
    private:
        //! parser of values
        args::stream_parser<int> _val_parser;

        //! parser of commands
        args::sub_command _command_parser;

        //! temporary variable to save parsed values
        int _temp_val;

        //! total value
        int _total;

        //! whether this class shows the average of inputs
        bool _shows_average;

        //! number of inputs
        int _num_inputs;

    public:
        //! default constructor
        adder()
            : _val_parser(_temp_val),
              _command_parser("add"),
              _temp_val(0),
              _total(0),
              _shows_average(false),
              _num_inputs(0) {
            _command_parser.option_parser() =
                args::opt(_shows_average)["average"]["a"](
                    "show the average of inputs") |
                args::remaining(
                    [this](const std::string& str) {
                        _val_parser(str);
                        _total += _temp_val;
                        ++_num_inputs;
                    },
                    "<integer1> <integer2> <integer3> ...")(
                    "integers to be added")
                    .accepts_multiple_args(true);
            _command_parser.abstract("add integers");
        }

        //! get parser
        const args::sub_command& parser() const { return _command_parser; }

        //! process the result
        void process() const {
            if (!_command_parser.used()) {
                return;
            }
            std::cout << "Total: " << _total << std::endl;
            if (_shows_average) {
                std::cout << "Average: "
                          << static_cast<float>(_total) /
                        static_cast<float>(_num_inputs)
                          << std::endl;
            }
        }
    };

}  // namespace

int main(int argc, char* argv[]) {
    try {
        adder my_adder;

        bool print_help;
        int exit_code = EXIT_SUCCESS;
        auto parser = my_adder.parser() |
            args::opt(print_help)["help"]["h"]("print this help") |
            args::arg(exit_code, "<exit_code>")["exit_code"](
                "exit with given exit code");

        parser.exe_name("calc");
        parser.abstract("test command to perform some calculations");

        int ret = parser.parse(argc, argv);
        if (ret == EXIT_FAILURE) {
            std::cout << parser.error_str() << "\n\n" << parser;
            return ret;
        }
        if (print_help) {
            std::cout << parser;
            return EXIT_SUCCESS;
        }

        my_adder.process();
        return exit_code;
    } catch (std::exception& e) {
        std::cout << "uncaught exception: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}