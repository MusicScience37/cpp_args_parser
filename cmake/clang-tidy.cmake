set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if (CPPARGSPARSER_STANDALONE)
option(CPPARGSPARSER_ENABLE_CLANG_TIDY "enable clang-tidy" ON)
else()
option(CPPARGSPARSER_ENABLE_CLANG_TIDY "enable clang-tidy" OFF)
endif()

find_program(CPPARGSPARSER_CLANG_TIDY clang-tidy)

set(CPPARGSPARSER_COMPILE_COMMANDS "${CMAKE_BINARY_DIR}/compile_commands.json")

# target for clang-tidy of all targets
add_custom_target(CppArgsParser_clang_tidy)
# set directory for output
set(CPPARGSPARSER_CLANG_TIDY_RESULTS_DIR "${CMAKE_BINARY_DIR}/clang_tidy")
file(MAKE_DIRECTORY ${CPPARGSPARSER_CLANG_TIDY_RESULTS_DIR})

function(clang_tidy target)
    if (CPPARGSPARSER_ENABLE_CLANG_TIDY)
        if (CPPARGSPARSER_CLANG_TIDY)
            # target name
            set(CLANG_TIDY_TARGET "${target}_clang_tidy")
            # make the list of source codes
            get_target_property(TARGET_SOURCES ${target} SOURCES)
            list(FILTER TARGET_SOURCES INCLUDE REGEX ".[cpp|h]$")
            # file name of results
            set(RESULT_PATH "${CPPARGSPARSER_CLANG_TIDY_RESULTS_DIR}/${CLANG_TIDY_TARGET}.txt")
            if (WIN32)
                # command to execute clang-tidy
                add_custom_command(
                    OUTPUT ${RESULT_PATH}
                    COMMAND "${CPPARGSPARSER_CLANG_TIDY}" "-config=" "--quiet"
                        "-p=${CPPARGSPARSER_COMPILE_COMMANDS}" ${TARGET_SOURCES}
                        > ${RESULT_PATH}
                    DEPENDS ${TARGET_SOURCES}
                    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                    COMMENT "make clang-tidy result for ${target}"
                )
                # command to show the result of clang-tidy
                add_custom_target(
                    ${CLANG_TIDY_TARGET}
                    COMMAND type "${CLANG_TIDY_TARGET}.txt"
                    DEPENDS ${RESULT_PATH}
                    WORKING_DIRECTORY ${CPPARGSPARSER_CLANG_TIDY_RESULTS_DIR}
                    COMMENT "show clang-tidy result for ${target}"
                )
            else()
                # command to execute clang-tidy
                add_custom_command(
                    OUTPUT ${RESULT_PATH}
                    COMMAND "${CPPARGSPARSER_CLANG_TIDY}" "-config=" "--quiet"
                        "-p=${CPPARGSPARSER_COMPILE_COMMANDS}" ${TARGET_SOURCES}
                        | tee ${RESULT_PATH}
                    DEPENDS ${TARGET_SOURCES}
                    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                )
                # command to show the result of clang-tidy
                add_custom_target(
                    ${CLANG_TIDY_TARGET}
                    DEPENDS ${RESULT_PATH}
                    WORKING_DIRECTORY ${CPPARGSPARSER_CLANG_TIDY_RESULTS_DIR}
                )
            endif()
            # add dependencies
            add_dependencies(${target} ${CLANG_TIDY_TARGET})
            add_dependencies(CppArgsParser_clang_tidy ${CLANG_TIDY_TARGET})
        endif()
    endif()
endfunction()
