#!/bin/bash

lines=($(cat $1/*_clang_tidy.txt | wc -l))

if  [ "$lines" = "0" ]; then
    echo "no warnings in clang-tidy"
    exit 0
else
    echo "some warnings in clang-tidy"
    cat $1/*_clang_tidy.txt
    exit 1
fi
