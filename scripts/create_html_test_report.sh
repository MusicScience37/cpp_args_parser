#!/bin/bash

# usage: create_html_test_report.sh <directory in which JUnit XMLs exist>

DIR=$1

python3 $(which junitparser) merge $DIR/CppArgsParser_*.xml $DIR/CppArgsParser_tests_unified.xml
python3 -m junit2htmlreport $DIR/CppArgsParser_tests_unified.xml $DIR/CppArgsParser_tests_unified.html
