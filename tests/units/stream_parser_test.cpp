/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/stream_parser.h"

#include <catch2/catch.hpp>
#include <sstream>

namespace {
    struct Dummy {};
}  // namespace

TEST_CASE("args::impl::supports_istream") {
    using args::impl::supports_istream;

    SECTION("check the result") {
        REQUIRE(supports_istream<int>::value == true);
        REQUIRE(supports_istream<unsigned int>::value == true);
        REQUIRE(supports_istream<long>::value == true);
        REQUIRE(supports_istream<std::string>::value == true);
        REQUIRE(supports_istream<Dummy>::value == false);
    }
}

TEST_CASE("args::stream_parser") {
    SECTION("integer value") {
        int result;
        args::stream_parser<int> parser(result);

        REQUIRE_NOTHROW(parser("123"));
        REQUIRE(result == 123);  // NOLINT: magic number for test

        REQUIRE_NOTHROW(parser("-123"));
        REQUIRE(result == -123);  // NOLINT: magic number for test

        REQUIRE_THROWS_WITH(parser("abc"), "couldn't parse an argument: abc");
        REQUIRE_THROWS_WITH(parser("123 "), "couldn't parse an argument: 123 ");
        REQUIRE_THROWS_WITH(
            parser("123 abc"), "couldn't parse an argument: 123 abc");
    }

    SECTION("string") {
        std::string result;
        args::stream_parser<std::string> parser(result);

        REQUIRE_NOTHROW(parser("abc"));
        REQUIRE(result == "abc");

        REQUIRE_NOTHROW(parser("123 abc"));
        REQUIRE(result == "123 abc");
    }
}