/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/combined_parser.h"

#include <catch2/catch.hpp>
#include <sstream>

#include "args/arg.h"
#include "args/opt.h"
#include "args/remaining.h"

TEST_CASE("args::combined_parser") {
    SECTION("default constructor") {
        args::combined_parser parser;
        // no option is added, so fail to parse anything
        std::vector<std::string> args = {"abc", "123"};
        REQUIRE(parser.parse(args) == EXIT_FAILURE);
    }

    SECTION("add an option") {
        args::opt option;
        option.specifier("test");
        args::combined_parser parser;
        parser.push_back(option);

        std::vector<std::string> args = {"abc", "--test"};
        REQUIRE(parser.parse(args) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }

    SECTION("add multiple options") {
        args::opt test_option;
        test_option.specifier("test");
        args::arg arg_option;
        arg_option.specifier("arg");

        args::combined_parser parser;
        parser.push_back(test_option).push_back(arg_option);

        std::vector<std::string> args = {"abc", "--test", "--arg", "123"};
        REQUIRE(parser.parse(args) == EXIT_SUCCESS);
        REQUIRE(test_option.found() == true);
        REQUIRE(arg_option.arguments().size() == 1);
        REQUIRE(arg_option.arguments()[0] == "123");
    }

    SECTION("operator to add an option") {
        bool has_test = false;
        args::combined_parser parser;
        parser |= args::opt(has_test)["test"];

        std::vector<std::string> args = {"abc", "--test"};
        REQUIRE(parser.parse(args) == EXIT_SUCCESS);
        REQUIRE(has_test == true);
    }

    SECTION("operator to combine two options") {
        args::opt test_option;
        test_option["test"]("test of an option");
        args::arg arg_option;
        arg_option["arg"](
            "test of an option with an argument\n"
            "(an integer)");

        auto parser = test_option | arg_option;

        std::vector<std::string> args = {"abc", "--test", "--arg", "123"};
        REQUIRE(parser.parse(args) == EXIT_SUCCESS);
        REQUIRE(test_option.found() == true);
        REQUIRE(arg_option.arguments().size() == 1);
        REQUIRE(arg_option.arguments()[0] == "123");
    }

    SECTION("set the name of the executable") {
        args::opt test_option;
        args::arg arg_option;

        auto parser = test_option | arg_option;
        parser.exe_name("test");

        REQUIRE(test_option.exe_name() == "test");
        REQUIRE(arg_option.exe_name() == "test");
    }

    SECTION("print help") {
        bool has_test = false;
        int arg = 0;
        bool has_flag = false;
        auto parser = args::opt(has_test)["test"]['t']("test of an option") |
            args::arg(arg, "<integer>")["arg"](
                "test of an option with an argument\n"
                "(an integer)") |
            args::opt(has_flag)["flag"]("another flag for test of options");

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test, -t                  test of an option\n"
            "  --arg <integer>             test of an option with an argument\n"
            "                              (an integer)\n"
            "  --flag                      another flag for test of options\n");
    }

    SECTION("another order of combination of options") {
        bool has_test = false;
        int arg = 0;
        bool has_flag = false;
        auto parser = args::opt(has_test)["test"]['t']("test of an option") |
            (args::arg(arg, "<integer>")["arg"](
                 "test of an option with an argument\n"
                 "(an integer)") |
                args::opt(has_flag)["flag"](
                    "another flag for test of options"));

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test, -t                  test of an option\n"
            "  --arg <integer>             test of an option with an argument\n"
            "                              (an integer)\n"
            "  --flag                      another flag for test of options\n");
    }

    SECTION("another combination of options") {
        bool has_test = false;
        int arg = 0;
        bool has_flag = false;
        std::string name;
        auto parser = (args::opt(has_test)["test"]['t']("test of an option") |
                          args::arg(name, "<name>")["name"](
                              "test of an option with an argument")) |
            (args::arg(arg, "<integer>")["arg"](
                 "test of an option with an argument\n"
                 "(an integer)") |
                args::opt(has_flag)["flag"](
                    "another flag for test of options"));

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test, -t                  test of an option\n"
            "  --name <name>               test of an option with an argument\n"
            "  --arg <integer>             test of an option with an argument\n"
            "                              (an integer)\n"
            "  --flag                      another flag for test of options\n");
    }

    SECTION("implicit convertion from a parser") {
        args::combined_parser parser;
        REQUIRE(parser.parse({"abc", "--test"}) == EXIT_FAILURE);

        bool has_test = false;
        parser = args::opt(has_test)["test"];
        REQUIRE(parser.parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(has_test == true);
    }

    SECTION("combine remaining parsers") {
        bool has_test = false;
        int arg1 = 0;
        int arg2 = 0;
        auto parser = args::opt(has_test)["test"]("test of an option") |
            args::remaining(arg1, "<arg1>")("first argument") |
            args::remaining(arg2, "<arg2>")("second argument");

        REQUIRE(parser.parse({"abc", "123", "--test", "456"}) == EXIT_SUCCESS);
        REQUIRE(has_test == true);
        REQUIRE(arg1 == 123);  // NOLINT: magic number for test
        REQUIRE(arg2 == 456);  // NOLINT: magic number for test

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test                      test of an option\n"
            "  <arg1>                      first argument\n"
            "  <arg2>                      second argument\n");

        REQUIRE(parser.remaining_arguments_hints() == "<arg1> <arg2>");
    }

    SECTION("make a clone") {
        args::opt test_option;
        test_option["test"]("test of an option");
        args::arg arg_option;
        arg_option["arg"](
            "test of an option with an argument\n"
            "(an integer)");

        auto ptr = (test_option | arg_option).clone();

        std::vector<std::string> args = {"abc", "--test", "--arg", "123"};
        REQUIRE(ptr->parse(args) == EXIT_SUCCESS);
        REQUIRE(test_option.found() == true);
        REQUIRE(arg_option.arguments().size() == 1);
        REQUIRE(arg_option.arguments()[0] == "123");
    }
}
