/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/help_util.h"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE("args::impl::print_spaces") {
    using args::impl::print_spaces;
    std::ostringstream ost;

    SECTION("print 2 spaces") {
        print_spaces(ost, 2);
        REQUIRE(ost.str() == "  ");
    }

    SECTION("print 4 spaces") {
        print_spaces(ost, 4);
        REQUIRE(ost.str() == "    ");
    }
}

TEST_CASE("args::impl::print_with_indent") {
    using args::impl::print_with_indent;
    std::ostringstream ost;

    SECTION("print with an indent") {
        print_with_indent(ost,
            "test1\n"
            "test2\n"
            "test3",
            2);
        REQUIRE(ost.str() ==
            "  test1\n"
            "  test2\n"
            "  test3");
    }

    SECTION("print with different indent width at first") {
        print_with_indent(ost,
            "test1\n"
            "test2\n"
            "test3",
            2, 1);
        REQUIRE(ost.str() ==
            " test1\n"
            "  test2\n"
            "  test3");
    }
}

TEST_CASE("args::impl::print_description") {
    using args::impl::print_description;
    std::ostringstream ost;

    SECTION("print after short specifiers") {
        print_description(ost,
            "test1\n"
            "test2",
            23);  // NOLINT: magic number for test
        REQUIRE(ost.str() ==
            "     "  // 5 spaces
            "test1\n"
            "                              "  // 30 spaces
            "test2\n");
    }

    SECTION("print after specifiers with a length on the border") {
        print_description(ost,
            "test1\n"
            "test2",
            24);  // NOLINT: magic number for test
        REQUIRE(ost.str() ==
            "    "  // 4 spaces
            "test1\n"
            "                              "  // 30 spaces
            "test2\n");
    }

    SECTION("print after long specifiers") {
        print_description(ost,
            "test1\n"
            "test2",
            25);  // NOLINT: magic number for test
        REQUIRE(ost.str() ==
            "    "  // 4 spaces
            "test1\n"
            "                              "  // 30 spaces
            "test2\n");
    }
}
