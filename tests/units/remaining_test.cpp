/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/remaining.h"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE("args::remaining") {
    SECTION("parse without callback") {
        args::remaining parser;
        REQUIRE(parser.found() == false);
        REQUIRE(parser.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(parser.found() == true);
        REQUIRE(parser.arguments().size() == 1);
        REQUIRE(parser.arguments()[0] == "test");
    }

    SECTION("parse with integer argument") {
        int arg = 0;
        args::remaining parser(arg);
        REQUIRE(parser.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(arg == 0);
        REQUIRE(parser.parse({"abc", "123"}) == EXIT_SUCCESS);
        REQUIRE(arg == 123);  // NOLINT: magic number for test
    }

    SECTION("parse error with integer argument") {
        int arg = 0;
        args::remaining parser(arg);
        REQUIRE(parser.parse({"abc", "abc"}) == EXIT_FAILURE);
        REQUIRE(parser.parse({"abc", "123 "}) == EXIT_FAILURE);
        REQUIRE(parser.parse({"abc", "123 abc"}) == EXIT_FAILURE);
    }

    SECTION("parse with string argument") {
        std::string arg;
        args::remaining parser(arg);
        REQUIRE(parser.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(arg == "test");
    }

    SECTION("parse with callback") {
        std::vector<std::string> arg;
        args::remaining parser(
            [&arg](const std::string& str) { arg.push_back(str); });
        REQUIRE(parser.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(arg.size() == 1);
        REQUIRE(arg[0] == "test");
    }

    SECTION("parse error with unacceptable multiple arguments") {
        args::remaining parser;
        REQUIRE(parser.parse({"abc", "test", "123"}) == EXIT_FAILURE);
        REQUIRE(parser.error_str() == "unexpected argument: 123");
        REQUIRE(parser.arguments().size() == 1);
        REQUIRE(parser.arguments()[0] == "test");
    }

    SECTION("parse error with options") {
        args::remaining parser;
        REQUIRE(parser.parse({"abc", "--test"}) == EXIT_FAILURE);
        REQUIRE(parser.error_str() == "unexpected argument: --test");
    }

    SECTION("parse multiple arguments") {
        args::remaining parser;
        parser.accepts_multiple_args(true);
        REQUIRE(parser.parse({"abc", "test", "123"}) == EXIT_SUCCESS);
        REQUIRE(parser.arguments().size() == 2);
        REQUIRE(parser.arguments()[0] == "test");
        REQUIRE(parser.arguments()[1] == "123");
    }

    SECTION("print help") {
        args::remaining parser;
        parser.description("argument");
        REQUIRE(parser.remaining_arguments_hints() == "<argument>");

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() == "  <argument>                  argument\n");
    }

    SECTION("print help with settings") {
        args::remaining parser;
        parser("some integer").hint("<integer>");
        REQUIRE(parser.remaining_arguments_hints() == "<integer>");

        std::ostringstream ost;
        parser.print_option_description(ost);
        REQUIRE(ost.str() == "  <integer>                   some integer\n");
    }

    SECTION("clone") {
        args::remaining parser;
        auto ptr = parser.clone();

        REQUIRE(ptr->parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(parser.arguments().size() == 1);
        REQUIRE(parser.arguments()[0] == "test");
    }
}
