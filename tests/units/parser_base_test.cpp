/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/parser_base.h"

#include <catch2/catch.hpp>
#include <sstream>

namespace {

    class test_parser : public args::parser_base {
    protected:
        std::size_t parse_impl(
            const args::args_holder& args, std::size_t ind) override {
            switch (action) {
            case action_type::return_one:
                used_args.push_back(args[ind]);
                return 1;
            case action_type::return_zero:
                return 0;
            case action_type::throw_parse_error:
                throw args::parse_error("parse error for test");
            case action_type::throw_runtime_error:
                throw std::runtime_error("runtime error for test");
            }
            throw std::runtime_error("unknown test type");
        }

    public:
        void print_option_description(std::ostream& stream) const override {
            stream << "help message for test" << std::endl;
        }

        std::string remaining_arguments_hints() const override {
            return "<arg>";
        }

        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<test_parser>(*this);
        }

        enum class action_type {
            return_one,
            return_zero,
            throw_parse_error,
            throw_runtime_error
        };

        action_type action{action_type::return_one};

        std::vector<std::string> used_args{};
    };

}  // namespace

TEST_CASE("args::parser_base") {
    test_parser parser;

    SECTION("parse arguments successfully") {
        const std::vector<std::string> args_str = {"abc", "123"};
        parser.action = test_parser::action_type::return_one;
        REQUIRE(parser.parse(args_str) == EXIT_SUCCESS);
        REQUIRE(parser.error_str().empty() == true);
        REQUIRE(parser.used_args.size() == 1);
        REQUIRE(parser.used_args[0] == "123");
    }

    SECTION("parse arguments successfully as in main function") {
        // ignore warnings for C-style arrays needed in main function
        char arg0[] = "abc";           // NOLINT
        char arg1[] = "123";           // NOLINT
        char* argv[2] = {arg0, arg1};  // NOLINT
        int argc = 2;

        parser.action = test_parser::action_type::return_one;
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT
        REQUIRE(parser.error_str().empty() == true);
        REQUIRE(parser.used_args.size() == 1);
        REQUIRE(parser.used_args[0] == "123");
    }

    SECTION("parse multiple arguments") {
        const std::vector<std::string> args_str = {"abc", "123", "456"};
        parser.action = test_parser::action_type::return_one;
        REQUIRE(parser.parse(args_str) == EXIT_SUCCESS);
        REQUIRE(parser.error_str().empty() == true);
        REQUIRE(parser.used_args.size() == 2);
        REQUIRE(parser.used_args[0] == "123");
        REQUIRE(parser.used_args[1] == "456");
    }

    SECTION("unexpected arguments") {
        const std::vector<std::string> args_str = {"abc", "123"};
        parser.action = test_parser::action_type::return_zero;
        REQUIRE(parser.parse(args_str) == EXIT_FAILURE);
        REQUIRE(parser.error_str() == "unexpected argument: 123");
    }

    SECTION("parse error") {
        const std::vector<std::string> args_str = {"abc", "123"};
        parser.action = test_parser::action_type::throw_parse_error;
        REQUIRE(parser.parse(args_str) == EXIT_FAILURE);
        REQUIRE(parser.error_str() == "parse error for test");
    }

    SECTION("runtime error") {
        const std::vector<std::string> args_str = {"abc", "123"};
        parser.action = test_parser::action_type::throw_runtime_error;
        REQUIRE(parser.parse(args_str) == EXIT_FAILURE);
        REQUIRE(parser.error_str() ==
            "error in parsing process: runtime error for test");
    }

    SECTION("print help") {
        std::ostringstream ost;
        parser.print_help(ost);
        REQUIRE(ost.str() ==
            "Usage\n"
            "  <path to me> [options] <arg>\n"
            "\n"
            "help message for test\n");

        parser.exe_name("test");
        REQUIRE(parser.exe_name() == "test");
        parser.abstract("abstract");

        ost.str("");
        ost << parser;
        REQUIRE(ost.str() ==
            "Usage\n"
            "  test [options] <arg>\n"
            "\n"
            "  abstract\n"
            "\n"
            "help message for test\n");
    }
}
