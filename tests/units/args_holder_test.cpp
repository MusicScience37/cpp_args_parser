/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/args_holder.h"

#include <catch2/catch.hpp>

TEST_CASE("args::args_holder") {
    SECTION("construct from a vector of arguments") {
        const std::vector<std::string> args_str = {"abc", "123", "456"};

        args::args_holder args(args_str);
        REQUIRE(args.size() == 3);
        REQUIRE(args[0] == "abc");
        REQUIRE(args[1] == "123");
        REQUIRE(args[2] == "456");
    }

    SECTION("construct from arguments as in main function") {
        // ignore warnings for C-style arrays needed in main function
        char arg0[] = "abc";                 // NOLINT
        char arg1[] = "123";                 // NOLINT
        char arg2[] = "456";                 // NOLINT
        char* argv[3] = {arg0, arg1, arg2};  // NOLINT
        int argc = 3;

        args::args_holder args(argc, argv);  // NOLINT
        REQUIRE(args.size() == 3);
        REQUIRE(args[0] == "abc");
        REQUIRE(args[1] == "123");
        REQUIRE(args[2] == "456");
    }
}