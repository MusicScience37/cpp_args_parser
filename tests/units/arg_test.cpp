/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/arg.h"

#include <catch2/catch.hpp>
#include <sstream>

// NOLINTNEXTLINE: tests to a single class shouldn't separated without a reason
TEST_CASE("args::arg") {
    SECTION("parse with no callback") {
        args::arg option;
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == false);
        REQUIRE(option.arguments().empty() == true);

        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");
    }

    SECTION("parse with string variable") {
        std::string value;
        args::arg option(value);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        REQUIRE(option.arguments().empty() == true);

        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_SUCCESS);
        REQUIRE(value == "argument");
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");
    }

    SECTION("parse with string variable with spaces") {
        std::string value;
        args::arg option(value);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        REQUIRE(option.arguments().empty() == true);

        REQUIRE(option.parse({"abc", "--test", "argument "}) == EXIT_SUCCESS);
        REQUIRE(value == "argument ");
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument ");
    }

    SECTION("parse with integer variable") {
        int value = 0;
        args::arg option(value);
        REQUIRE(value == 0);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(value == 0);
        REQUIRE(option.found() == false);
        REQUIRE(option.arguments().empty() == true);

        REQUIRE(option.parse({"abc", "--test", "123"}) == EXIT_SUCCESS);
        REQUIRE(value == 123);  // NOLINT: magic number for test
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "123");
    }

    SECTION("parse with callback function") {
        std::string value;
        auto callback = [&value](const std::string& str) { value = str; };
        args::arg option(callback);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(value.empty() == true);
        REQUIRE(option.found() == false);
        REQUIRE(option.arguments().empty() == true);

        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_SUCCESS);
        REQUIRE(value == "argument");
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");
    }

    SECTION("specifier with a single character") {
        args::arg option;
        option.specifier('a');

        REQUIRE(option.parse({"abc", "-a", "argument"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");
    }

    SECTION("specifier with a string made of a single character") {
        args::arg option;
        option.specifier("a");

        REQUIRE(option.parse({"abc", "-a", "argument"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");
    }

    SECTION("invalid specifier") {
        args::arg option;
        REQUIRE_THROWS(option.specifier(""));
    }

    SECTION("wrong specifiers in parse") {
        args::arg option;
        option.specifier("test");
        REQUIRE(option.parse({"abc", "-test"}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
    }

    SECTION("wrong arguments in parse") {
        int value = 0;
        args::arg option(value);
        option.specifier("test");
        REQUIRE(option.parse({"abc", "--test"}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
        REQUIRE(option.parse({"abc", "--test", ""}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
        REQUIRE(option.parse({"abc", "--test", "123 a"}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
    }

    SECTION("set multiple times") {
        args::arg option;
        option.specifier("test");

        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");

        REQUIRE(option.parse({"abc", "--test", "argument2"}) == EXIT_FAILURE);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 1);
        REQUIRE(option.arguments()[0] == "argument");

        option.usable_multiple_times(true);
        REQUIRE(option.parse({"abc", "--test", "argument2"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
        REQUIRE(option.arguments().size() == 2);
        REQUIRE(option.arguments()[1] == "argument2");
    }

    SECTION("multiple specifiers") {
        std::size_t called = 0;
        auto callback = [&called](const std::string& /*unused*/) { ++called; };
        args::arg option(callback);
        option.specifier("test")
            .specifier('t')
            .specifier("another")
            .usable_multiple_times(true);

        REQUIRE(called == 0);
        REQUIRE(option.parse({"abc", "--test", "argument"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "-t", "argument"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "--another", "argument"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "--test", "argument", "-t", "argument"}) ==
            EXIT_SUCCESS);
        REQUIRE(called == 2);

        called = 0;
        REQUIRE(option.parse({"abc", "--test", "argument", "-t", "argument",
                    "--another", "argument"}) == EXIT_SUCCESS);
        REQUIRE(called == 3);
    }

    SECTION("print help") {
        args::arg option;
        option.specifier("test").description("help message");
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() == "  --test <value>              help message\n");
    }

    SECTION("print help with custom hint string") {
        args::arg option;
        option.specifier("test").description("help message").hint("message");
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() == "  --test message              help message\n");
    }

    SECTION("print long help") {
        args::arg option;
        option.specifier("test-long-long-long-option")
            .description("help message\nwith two lines")
            .specifier('t');
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test-long-long-long-option <value>, -t <value>    help "
            "message\n"
            "                              with two lines\n");
    }

    SECTION("simple operators") {
        args::arg option;
        option["test"]("help message")['t'];
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() == "  --test <value>, -t <value>    help message\n");
    }

    SECTION("make a clone") {
        args::arg option;
        auto ptr = option.clone();
        // check whether two objects share the data of the same option
        option["test"];
        REQUIRE(ptr->parse({"abc", "--test", "123"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }
}