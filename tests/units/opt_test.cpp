/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/opt.h"

#include <catch2/catch.hpp>
#include <sstream>

TEST_CASE("args::opt") {
    SECTION("parse with no callback") {
        args::opt option;
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == false);

        REQUIRE(option.parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }

    SECTION("parse with bool variable") {
        bool found = true;
        args::opt option(found);
        REQUIRE(found == false);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(found == false);
        REQUIRE(option.found() == false);

        REQUIRE(option.parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(found == true);
        REQUIRE(option.found() == true);
    }

    SECTION("parse with callback function") {
        bool called = false;
        auto callback = [&called] { called = true; };
        args::opt option(callback);
        REQUIRE(called == false);
        REQUIRE(option.found() == false);
        option.specifier("test");

        REQUIRE(option.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(called == false);
        REQUIRE(option.found() == false);

        REQUIRE(option.parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(called == true);
        REQUIRE(option.found() == true);
    }

    SECTION("specifier with a single character") {
        args::opt option;
        option.specifier('a');

        REQUIRE(option.parse({"abc", "-a"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }

    SECTION("specifier with a string made of a single character") {
        args::opt option;
        option.specifier("a");

        REQUIRE(option.parse({"abc", "-a"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }

    SECTION("invalid specifier") {
        args::opt option;
        REQUIRE_THROWS(option.specifier(""));
    }

    SECTION("wrong specifiers in parse") {
        args::opt option;
        option.specifier("test");
        REQUIRE(option.parse({"abc", "-test"}) == EXIT_FAILURE);
        REQUIRE(option.found() == false);
    }

    SECTION("multiple specifiers") {
        std::size_t called = 0;
        auto callback = [&called] { ++called; };
        args::opt option(callback);
        option.specifier("test").specifier('t').specifier("another");

        REQUIRE(called == 0);
        REQUIRE(option.parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "-t"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "--another"}) == EXIT_SUCCESS);
        REQUIRE(called == 1);

        called = 0;
        REQUIRE(option.parse({"abc", "--test", "-t"}) == EXIT_SUCCESS);
        REQUIRE(called == 2);

        called = 0;
        REQUIRE(
            option.parse({"abc", "--test", "-t", "--another"}) == EXIT_SUCCESS);
        REQUIRE(called == 3);
    }

    SECTION("print help") {
        args::opt option;
        option.specifier("test").description("help message").specifier('t');
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() == "  --test, -t                  help message\n");
    }

    SECTION("print long help") {
        args::opt option;
        option.specifier("test-long-long-long-option")
            .description("help message\nwith two lines")
            .specifier('t');
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() ==
            "  --test-long-long-long-option, -t    help message\n"
            "                              with two lines\n");
    }

    SECTION("simple operators") {
        args::opt option;
        option["test"]("help message")['t'];
        std::ostringstream ost;
        option.print_option_description(ost);
        REQUIRE(ost.str() == "  --test, -t                  help message\n");
    }

    SECTION("make a clone") {
        args::opt option;
        auto ptr = option.clone();
        // check whether two objects share the data of the same option
        option["test"];
        REQUIRE(ptr->parse({"abc", "--test"}) == EXIT_SUCCESS);
        REQUIRE(option.found() == true);
    }
}