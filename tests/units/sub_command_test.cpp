/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include "args/sub_command.h"

#include <catch2/catch.hpp>
#include <sstream>

#include "args/arg.h"
#include "args/remaining.h"

TEST_CASE("args::sub_command") {
    SECTION("check of flag") {
        args::sub_command com("test");
        REQUIRE(com.is_sub_command() == true);
    }

    SECTION("parse with no option, no callback") {
        args::sub_command com("test");
        REQUIRE(com.used() == false);
        REQUIRE(com.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(com.used() == false);
        REQUIRE(com.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(com.used() == true);
    }

    SECTION("parse with bool variable") {
        bool used = true;
        args::sub_command com("test", used);
        REQUIRE(used == false);
        REQUIRE(com.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(used == false);
        REQUIRE(com.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(used == true);
    }

    SECTION("parse with callback function") {
        bool used = false;
        args::sub_command com("test", [&used]() { used = true; });
        REQUIRE(used == false);
        REQUIRE(com.parse({"abc"}) == EXIT_SUCCESS);
        REQUIRE(used == false);
        REQUIRE(com.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(used == true);
    }

    SECTION("parse with an option") {
        args::sub_command com("test");
        int arg = 0;
        com.option_parser() = args::arg(arg)["arg"];
        REQUIRE(com.parse({"abc", "test"}) == EXIT_SUCCESS);
        REQUIRE(com.parse({"abc", "test", "--arg", "123"}) == EXIT_SUCCESS);
        REQUIRE(arg == 123);
        REQUIRE(com.parse({"abc", "test", "--arg", "abc"}) == EXIT_FAILURE);
    }

    SECTION("parse error") {
        args::sub_command com("test");
        REQUIRE(com.parse({"abc", "test", "123"}) == EXIT_FAILURE);
        REQUIRE(com.error_str() == "unexpected argument: 123");
        REQUIRE(com.parse({"abc", "123"}) == EXIT_FAILURE);
        REQUIRE(com.error_str() == "unexpected argument: 123");
    }

    SECTION("print help") {
        args::sub_command com("test");
        com.option_parser() = args::arg()["arg"]("argument");

        std::ostringstream ost;
        com.print_option_description(ost);
        REQUIRE(ost.str() ==
            "Sub command 'test'\n"
            "  <path to me> test [options] \n"
            "\n"
            "  --arg <value>               argument\n");
    }

    SECTION("print help with custom settings") {
        args::sub_command com("test");
        com.option_parser() = args::arg()["arg"]("argument");
        com.exe_name("me");
        com.abstract("abstract");

        std::ostringstream ost;
        com.print_option_description(ost);
        REQUIRE(ost.str() ==
            "Sub command 'test'\n"
            "  me test [options] \n"
            "\n"
            "  abstract\n"
            "\n"
            "  --arg <value>               argument\n");
    }

    SECTION("print help with remaining arguments") {
        args::sub_command com("test");
        com.option_parser() = args::arg()["arg"]("argument") |
            args::remaining()("input").hint("<input>");

        std::ostringstream ost;
        com.print_option_description(ost);
        REQUIRE(ost.str() ==
            "Sub command 'test'\n"
            "  <path to me> test [options] <input>\n"
            "\n"
            "  --arg <value>               argument\n"
            "  <input>                     input\n");
    }

    SECTION("clone") {
        args::sub_command com("test");
        auto ptr = com.clone();
        int arg = 0;
        com.option_parser() = args::arg(arg)["arg"];
        REQUIRE(ptr->parse({"abc", "test", "--arg", "123"}) == EXIT_SUCCESS);
        REQUIRE(com.used() == true);
        REQUIRE(arg == 123);
    }
}
