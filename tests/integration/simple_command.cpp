/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <catch2/catch.hpp>

#include "args/args.h"

TEST_CASE("args_integration_test/simple_command") {
    bool has_test;
    int arg = 0;
    std::string rem1;
    std::string rem2;
    auto parser = args::opt(has_test)["test"]["t"]("an option") |
        args::arg(arg, "<integer>")["arg"](
            "an option with an argument\n(integer value)") |
        args::remaining(rem1, "<argument1>")("the first argument") |
        args::remaining(rem2, "<argument2>")("the second argument");
    parser.exe_name("command");

    SECTION("print help") {
        std::ostringstream ost;
        ost << parser;
        REQUIRE(ost.str() ==
            "Usage\n"
            "  command [options] <argument1> <argument2>\n"
            "\n"
            "  --test, -t                  an option\n"
            "  --arg <integer>             an option with an argument\n"
            "                              (integer value)\n"
            "  <argument1>                 the first argument\n"
            "  <argument2>                 the second argument\n");
    }

    SECTION("no option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char* argv[] = {arg0, nullptr};                     // NOLINT
        int argc = 1;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(has_test == false);
        REQUIRE(arg == 0);
        REQUIRE(rem1.empty() == true);
        REQUIRE(rem2.empty() == true);
    }

    SECTION("with an option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "--test";                             // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(has_test == true);
        REQUIRE(arg == 0);
        REQUIRE(rem1.empty() == true);
        REQUIRE(rem2.empty() == true);
    }

    SECTION("with an option with an argument") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "--arg";                              // NOLINT
        char arg2[] = "123";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, nullptr};         // NOLINT
        int argc = 3;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(has_test == false);
        REQUIRE(arg == 123);
        REQUIRE(rem1.empty() == true);
        REQUIRE(rem2.empty() == true);
    }

    SECTION("with additional arguments") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "-t";                                 // NOLINT
        char arg2[] = "123";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, nullptr};         // NOLINT
        int argc = 3;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(has_test == true);
        REQUIRE(arg == 0);
        REQUIRE(rem1 == "123");
        REQUIRE(rem2.empty() == true);
    }

    SECTION("with all features") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                                       // NOLINT
        char arg1[] = "abc";                                           // NOLINT
        char arg2[] = "--arg";                                         // NOLINT
        char arg3[] = "123";                                           // NOLINT
        char arg4[] = "def";                                           // NOLINT
        char arg5[] = "--test";                                        // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, arg4, arg5, nullptr};  // NOLINT
        int argc = 6;                                                  // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);             // NOLINT

        REQUIRE(has_test == true);
        REQUIRE(arg == 123);
        REQUIRE(rem1 == "abc");
        REQUIRE(rem2 == "def");
    }

    SECTION("too many additional arguments") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "abc";                                // NOLINT
        char arg2[] = "def";                                // NOLINT
        char arg3[] = "ghi";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, nullptr};   // NOLINT
        int argc = 4;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT
        REQUIRE(parser.error_str() == "unexpected argument: ghi");
    }

    SECTION("no option argument") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "--arg";                              // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT
        REQUIRE(parser.error_str() == "option --arg requires an argument");
    }

    SECTION("no option argument") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "--arg";                              // NOLINT
        char arg2[] = "abc";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, nullptr};         // NOLINT
        int argc = 3;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT
        REQUIRE(parser.error_str() == "couldn't parse an argument: abc");
    }

    SECTION("wrong option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "-test";                              // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT
        REQUIRE(parser.error_str() == "unexpected argument: -test");
    }
}