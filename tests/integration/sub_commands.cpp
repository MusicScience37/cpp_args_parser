/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#include <catch2/catch.hpp>

#include "args/args.h"

TEST_CASE("args_integration_test/sub_commands") {
    args::sub_command copy("copy");
    std::string copy_input;
    std::string copy_output;
    copy.option_parser() =
        args::remaining(copy_input, "<input>")("input file") |
        args::arg(copy_output, "<output>")["output"]["o"]("output file");
    copy.abstract("copy a file");

    args::sub_command list("list");
    args::remaining list_args;
    list_args.accepts_multiple_args(true).hint(
        "<element1> <element2> <element3> ...")("elements of the list");
    list.option_parser() = list_args;

    bool print_help = false;
    auto parser =
        copy | list | args::opt(print_help)["help"]["h"]("print this help");

    parser.exe_name("command");

    SECTION("print help") {
        std::ostringstream ost;
        ost << parser;
        REQUIRE(ost.str() ==
            "Usage\n"
            "  command [options] \n"
            "\n"
            "  --help, -h                  print this help\n"
            "\n"
            "Sub command 'copy'\n"
            "  command copy [options] <input>\n"
            "\n"
            "  copy a file\n"
            "\n"
            "  <input>                     input file\n"
            "  --output <output>, -o <output>    output file\n"
            "\n"
            "Sub command 'list'\n"
            "  command list [options] <element1> <element2> <element3> ...\n"
            "\n"
            "  <element1> <element2> <element3> ...    elements of the list\n");
    }

    SECTION("no option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char* argv[] = {arg0, nullptr};                     // NOLINT
        int argc = 1;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(print_help == false);
        REQUIRE(copy.used() == false);
        REQUIRE(list.used() == false);
    }

    SECTION("help option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "--help";                             // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(print_help == true);
        REQUIRE(copy.used() == false);
        REQUIRE(list.used() == false);
    }

    SECTION("copy command with no option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "copy";                               // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(print_help == false);
        REQUIRE(copy.used() == true);
        REQUIRE(copy_input.empty() == true);
        REQUIRE(copy_output.empty() == true);
        REQUIRE(list.used() == false);
    }

    SECTION("copy command with arguments") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                                 // NOLINT
        char arg1[] = "copy";                                    // NOLINT
        char arg2[] = "input.txt";                               // NOLINT
        char arg3[] = "-o";                                      // NOLINT
        char arg4[] = "output.txt";                              // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, arg4, nullptr};  // NOLINT
        int argc = 5;                                            // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);       // NOLINT

        REQUIRE(print_help == false);
        REQUIRE(copy.used() == true);
        REQUIRE(copy_input == "input.txt");
        REQUIRE(copy_output == "output.txt");
        REQUIRE(list.used() == false);
    }

    SECTION("copy command with too many arguments") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "copy";                               // NOLINT
        char arg2[] = "input.txt";                          // NOLINT
        char arg3[] = "list";                               // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, nullptr};   // NOLINT
        int argc = 4;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT

        REQUIRE(parser.error_str() == "unexpected argument: list");
        REQUIRE(print_help == false);
        REQUIRE(copy.used() == true);
        REQUIRE(list.used() == false);
    }

    SECTION("list command with no argument") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "list";                               // NOLINT
        char* argv[] = {arg0, arg1, nullptr};               // NOLINT
        int argc = 2;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(print_help == false);
        REQUIRE(copy.used() == false);
        REQUIRE(list.used() == true);
        REQUIRE(list_args.arguments().empty() == true);
    }

    SECTION("list command with no argument") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "list";                               // NOLINT
        char arg2[] = "abc";                                // NOLINT
        char arg3[] = "def";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, nullptr};   // NOLINT
        int argc = 4;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_SUCCESS);  // NOLINT

        REQUIRE(print_help == false);
        REQUIRE(copy.used() == false);
        REQUIRE(list.used() == true);
        REQUIRE(list_args.arguments().size() == 2);
        REQUIRE(list_args.arguments()[0] == "abc");
        REQUIRE(list_args.arguments()[1] == "def");
    }

    SECTION("list command with an unexpected option") {
        // ignore warning for use of C-style arguments
        char arg0[] = "command";                            // NOLINT
        char arg1[] = "list";                               // NOLINT
        char arg2[] = "-o";                                 // NOLINT
        char arg3[] = "def";                                // NOLINT
        char* argv[] = {arg0, arg1, arg2, arg3, nullptr};   // NOLINT
        int argc = 4;                                       // NOLINT
        REQUIRE(parser.parse(argc, argv) == EXIT_FAILURE);  // NOLINT

        REQUIRE(parser.error_str() == "unexpected argument: -o");
        REQUIRE(print_help == false);
        REQUIRE(copy.used() == false);
        REQUIRE(list.used() == true);
    }
}
