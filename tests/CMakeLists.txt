add_library(CppArgsParser_Catch2Main OBJECT catch2_main.cpp)
target_link_libraries(CppArgsParser_Catch2Main PUBLIC Catch2::Catch2)

# this variable must be cached for use in CTest, otherwise tests can't run
set(CPPARGSPARSER_TEMP_TEST_DIR "${CMAKE_BINARY_DIR}/temp_test"
    CACHE PATH "temporary directory to use in tests" FORCE)
file(MAKE_DIRECTORY ${CPPARGSPARSER_TEMP_TEST_DIR})

option(CPPARGSPARSER_CATCH2_WRITE_JUNIT "write test results to junit xml" OFF)

macro(add_catch2_test target)
    target_link_libraries(${target} PRIVATE
        CppArgsParser_Catch2Main
        CppArgsParser)
    clang_tidy(${target})

    if (CPPARGSPARSER_CATCH2_WRITE_JUNIT)
        set(CPPARGSPARSER_CATCH2_RESULTS_DIR "${CMAKE_BINARY_DIR}/Catch2_Results"
            CACHE PATH "directory of results of unit tests by Catch2" FORCE)
        file(MAKE_DIRECTORY ${CPPARGSPARSER_CATCH2_RESULTS_DIR})
        set(CATCH2_REPORT_OPTIONS -r junit -o ${CPPARGSPARSER_CATCH2_RESULTS_DIR}/${target}.xml)
    else()
        set(CATCH2_REPORT_OPTIONS -r compact)
    endif()

    add_test(
        NAME ${target} 
        COMMAND ${target} ${CATCH2_REPORT_OPTIONS}
        WORKING_DIRECTORY ${CPPARGSPARSER_TEMP_TEST_DIR})
endmacro()

add_subdirectory(units)
add_subdirectory(integration)