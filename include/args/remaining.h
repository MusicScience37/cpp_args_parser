/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <functional>
#include <memory>
#include <sstream>
#include <string>

#include "parser_base.h"
#include "stream_parser.h"

namespace args {

    /*!
     * \brief class to parse remaining arguments
     */
    class remaining : public parser_base {
    private:
        //! struct for data
        struct remaining_data_struct {  // NOLINT
                                        // initialization is done by remaining
                                        // class

            //! description of this option
            std::string description;

            //! hint string to specify what type of value should be inserted
            std::string hint;

            //! callback function to be called if a remaining arguments is found
            std::function<void(const std::string&)> callback;

            //! flag whether multiple arguments can be accepted
            bool accepts_multiple_args;

            //! parsed arguments
            std::vector<std::string> arguments;
        };

        //! data
        std::shared_ptr<remaining_data_struct> _data;

    public:
        /*!
         * \brief default constructor
         */
        remaining() : remaining([](const std::string& /*unused*/) {}) {}

        /*!
         * \brief construct with a reference to the result
         *
         * This function uses SFINAE to prevent compile errors
         * when callback function is used (use of other constructors).
         *
         * \tparam T type of arguments
         * \param argument reference to which this class writes parsed
         *        argument
         * \param hint hint string to specify what type of value should be
         *        inserted
         */
        template <typename T,
            typename std::enable_if<impl::supports_istream<T>::value,
                int>::type = 0>
        explicit remaining(T& argument, const std::string& hint = "<argument>")
            : remaining(stream_parser<T>(argument), hint) {}

        /*!
         * \brief construct with a callback function
         *
         * \param callback callback function to be called if a remaining
         *        arguments is found
         * \param hint hint string to specify what type of value should be
         *        inserted
         */
        explicit remaining(
            const std::function<void(const std::string&)>& callback,
            const std::string& hint = "<argument>")
            : _data(std::make_shared<remaining_data_struct>()) {
            _data->hint = hint;
            _data->callback = callback;
            _data->accepts_multiple_args = false;
        }

        //! default copy constructor
        remaining(const remaining&) noexcept = default;

        //! default copy assignment operator
        remaining& operator=(const remaining&) noexcept = default;

        //! default move constructor
        remaining(remaining&&) noexcept = default;

        //! default move assignment operator
        remaining& operator=(remaining&&) noexcept = default;

        //! default destructor
        ~remaining() override = default;

        /*!
         * \brief set description of this argument
         *
         * This class adds indents after each line feed characters.
         *
         * \param str description string
         * \return opt& this object
         */
        remaining& description(const std::string& str) {
            _data->description = str;
            return *this;
        }

        //! \copydoc description(const std::string& str)
        remaining& operator()(const std::string& str) {
            return description(str);
        }

        /*!
         * \brief set hint string to specify what type of value should be
         * inserted
         *
         * \param str hint string
         * \return arg& this object
         */
        remaining& hint(const std::string& str) {
            _data->hint = str;
            return *this;
        }

        /*!
         * \brief set whether multiple arguments can be accepted
         *
         * \param val whether multiple arguments can be accepted
         * \return remaining& this object
         */
        remaining& accepts_multiple_args(bool val) {
            _data->accepts_multiple_args = val;
            return *this;
        }

        /*!
         * \brief get whether an argument is found
         *
         * \return bool whether an argument is found
         */
        bool found() const { return !_data->arguments.empty(); }

        /*!
         * \brief get found arguments
         *
         * \return const std::vector<std::string>& vector of arguments
         */
        const std::vector<std::string>& arguments() const {
            return _data->arguments;
        }

    protected:
        /*!
         * \brief parse an argument
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        std::size_t parse_impl(
            const args_holder& args, std::size_t ind) override {
            if ((!_data->arguments.empty()) &&
                (!_data->accepts_multiple_args)) {
                return 0;
            }
            // ignore options
            if (args[ind][0] == '-') {
                return 0;
            }
            _data->callback(args[ind]);
            _data->arguments.push_back(args[ind]);
            return 1;
        }

    public:
        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        void print_option_description(std::ostream& stream) const override {
            impl::print_spaces(stream, indents::before_commands);
            stream << _data->hint;
            impl::print_description(
                stream, _data->description, _data->hint.size());
        }

        /*!
         * \brief get hint string of remaining arguments to print in usage
         *        messages
         *
         * \return std::string hint string
         */
        std::string remaining_arguments_hints() const override {
            return _data->hint;
        }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<remaining>(*this);
        }
    };

}  // namespace args
