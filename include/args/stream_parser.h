/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>

#include "common.h"

namespace args {
    namespace impl {

        /*!
         * \brief implementation details of supports_istream
         */
        struct supports_istream_impl {
            template <typename T>
            static auto check(T*) -> decltype(
                (std::declval<std::istream&>() >> std::declval<T&>()),
                std::true_type());

            template <typename T>
            static auto check(...) -> std::false_type;
        };

        /*!
         * \brief check whether the given type supports std::istream
         *
         * \tparam T type to check
         */
        template <typename T>
        struct supports_istream
            : decltype(supports_istream_impl::check<T>(nullptr)) {};

    }  // namespace impl

    /*!
     * \brief parse strings using std::istream
     *
     * \tparam T type of the resulting value
     */
    template <typename T>
    class stream_parser {
        static_assert(impl::supports_istream<T>::value,
            "type T must support std::istream");

    private:
        //! reference to which this class writes the result
        T& _result;

    public:
        //! construct with a reference
        explicit stream_parser(T& result) : _result(result) {}

        //! parse a string
        void operator()(const std::string& str) {
            std::istringstream ist;
            ist.str(str);
            ist >> _result;
            if (!ist) {
                throw parse_error("couldn't parse an argument: " + str);
            }
            std::string remaining;
            std::getline(ist, remaining);
            if (!remaining.empty()) {
                throw parse_error("couldn't parse an argument: " + str);
            }
        }
    };

    /*!
     * \brief stream_parser for std::string
     *
     * This class simply passes the argument to the resulting string
     */
    template <>
    class stream_parser<std::string> {
    private:
        //! reference to which this class writes the result
        std::string& _result;

    public:
        //! construct with a reference
        explicit stream_parser(std::string& result) : _result(result) {}

        //! parse a string
        void operator()(const std::string& str) { _result = str; }
    };

}  // namespace args
