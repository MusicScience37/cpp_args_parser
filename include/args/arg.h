/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <functional>
#include <sstream>
#include <stdexcept>
#include <string>

#include "parser_base.h"
#include "stream_parser.h"

namespace args {

    /*!
     * \brief class to parse options with arguments
     */
    class arg : public parser_base {
    private:
        //! struct for data
        struct arg_data_struct {  // NOLINT
                                  // initialization is done by arg class

            //! specifiers of this option
            std::vector<std::string> specifiers;

            //! description of this option
            std::string description;

            //! hint string to specify what type of value should be inserted
            std::string hint;

            //! callback function to be called if this option is found
            std::function<void(const std::string&)> option_callback;

            //! flag whether this option can be set multiple times
            bool usable_multiple_times;

            //! parsed arguments
            std::vector<std::string> arguments;
        };

        //! data
        std::shared_ptr<arg_data_struct> _data;

    public:
        /*!
         * \brief construct an option with no callback
         */
        arg() : arg([](const std::string& /*unused*/) {}) {}

        /*!
         * \brief construct an option with a reference to the result
         *
         * This function uses SFINAE to prevent compile errors
         * when callback function is used (use of other constructors).
         *
         * \tparam T type of values in arguments
         * \param value a reference to which this class writes the argument
         * \param hint hint string to specify what type of value should be
         *        inserted
         */
        template <typename T,
            typename std::enable_if<impl::supports_istream<T>::value,
                int>::type = 0>
        explicit arg(T& value,  // NOLINT
                                // clang-tidy wrongly shows an warning here
            const std::string& hint = "<value>")
            : arg(stream_parser<T>(value), hint) {}

        /*!
         * \brief constrcut an option with a callback function
         *
         * \param callback callback function called with an argument
         *        when the option is found
         * \param hint hint string to specify what type of value should be
         *        inserted
         */
        explicit arg(std::function<void(const std::string& str)> callback,
            const std::string& hint = "<value>")
            : parser_base(), _data(std::make_shared<arg_data_struct>()) {
            _data->hint = hint;
            _data->option_callback = std::move(callback);
            _data->usable_multiple_times = false;
        }

        //! default copy constructor
        arg(const arg&) noexcept = default;

        //! default copy assignment operator
        arg& operator=(const arg&) noexcept = default;

        //! default move constructor
        arg(arg&&) noexcept = default;

        //! default move assignment operator
        arg& operator=(arg&&) noexcept = default;

        //! default destructor
        ~arg() override = default;

        /*!
         * \brief add a specifier to this option
         *
         * This function adds a specifier like `--name` (`name` is input string)
         * if input string is a string with its size longer than 2.
         * This function adds a specifier like `-a` (`a` is input string)
         * if input string is a single character.
         *
         * \param str string to make a specifier from
         * \return opt& this object
         */
        arg& specifier(const std::string& str) {
            if (str.empty()) {
                throw std::invalid_argument(
                    "input for args::opt::name must not be an empty string");
            }
            if (str.size() == 1) {
                return specifier(str[0]);
            }
            _data->specifiers.emplace_back("--" + str);
            return *this;
        }

        /*!
         * \brief add a specifier to this option
         *
         * This function adds a specifier like `-a` (`a` is input character).
         *
         * \param c character to make a specifier from
         * \return opt& this object
         */
        arg& specifier(char c) {
            _data->specifiers.emplace_back(std::string() + "-" + c);
            return *this;
        }

        //! \copydoc specifier(const std::string& str)
        arg& operator[](const std::string& str) { return specifier(str); }

        //! \copydoc specifier(char c)
        arg& operator[](char c) { return specifier(c); }

        /*!
         * \brief set description of this option
         *
         * This class adds indents after each line feed characters.
         *
         * \param str description string
         * \return opt& this object
         */
        arg& description(const std::string& str) {
            _data->description = str;
            return *this;
        }

        //! \copydoc description(const std::string& str)
        arg& operator()(const std::string& str) { return description(str); }

        /*!
         * \brief set hint string to specify what type of value should be
         * inserted
         *
         * \param str hint string
         * \return arg& this object
         */
        arg& hint(const std::string& str) {
            _data->hint = str;
            return *this;
        }

        /*!
         * \brief set whether this option can be set multiple times
         *
         * \param val flag whether this option can be set multiple times
         * \return arg& this object
         */
        arg& usable_multiple_times(bool val) {
            _data->usable_multiple_times = val;
            return *this;
        }

        /*!
         * \brief get whether this option is found
         *
         * \return bool whether this option is found
         */
        bool found() const { return !_data->arguments.empty(); }

        /*!
         * \brief get found arguments
         *
         * \return const std::vector<std::string>& vector of arguments
         */
        const std::vector<std::string>& arguments() const {
            return _data->arguments;
        }

    protected:
        /*!
         * \brief parse an argument
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        std::size_t parse_impl(
            const args_holder& args, std::size_t ind) override {
            for (const std::string& specifier : _data->specifiers) {
                if (args[ind] == specifier) {
                    ++ind;
                    if (ind >= args.size()) {
                        throw parse_error(
                            "option " + specifier + " requires an argument");
                    }
                    if ((!_data->arguments.empty()) &&
                        (!_data->usable_multiple_times)) {
                        throw parse_error("option " + specifier +
                            " cannot be set multiple times");
                    }
                    _data->option_callback(
                        args[ind]);  // may throw an exception
                    _data->arguments.push_back(args[ind]);
                    return 2;
                }
            }
            return 0;
        }

    public:
        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        void print_option_description(std::ostream& stream) const override {
            impl::print_spaces(stream, indents::before_commands);
            std::size_t commands_length = 0;

            for (std::size_t i = 0; i < _data->specifiers.size() - 1; ++i) {
                stream << _data->specifiers[i] << " " << _data->hint << ", ";
                commands_length +=
                    _data->specifiers[i].size() + _data->hint.size() + 3;
            }
            {
                const std::size_t i = _data->specifiers.size() - 1;
                stream << _data->specifiers[i] << " " << _data->hint;
                commands_length +=
                    _data->specifiers[i].size() + _data->hint.size() + 1;
            }

            impl::print_description(
                stream, _data->description, commands_length);
        }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<arg>(*this);
        }
    };

}  // namespace args
