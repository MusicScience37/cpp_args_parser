/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <iostream>
#include <string>

#include "common.h"

namespace args {
    namespace impl {

        /*!
         * \brief print spaces to a stream
         *
         * \param stream output stream
         * \param count number of spaces
         */
        inline void print_spaces(std::ostream& stream, std::size_t count) {
            for (std::size_t i = 0; i < count; ++i) {
                stream.put(' ');
            }
        }

        /*!
         * \brief print string with indents
         *
         * \param stream output stream
         * \param str string printed to stream
         * \param indent number of spaces for each line
         * \param first_indent number of spaces for the first line
         */
        inline void print_with_indent(std::ostream& stream, const std::string& str,
            std::size_t indent, std::size_t first_indent) {
            print_spaces(stream, first_indent);
            for (char c : str) {
                stream.put(c);
                if (c == '\n') {
                    print_spaces(stream, indent);
                }
            }
        }

        /*!
         * \brief print string with indents
         *
         * \param stream output stream
         * \param str string printed to stream
         * \param indent number of spaces for each line
         */
        inline void print_with_indent(
            std::ostream& stream, const std::string& str, std::size_t indent) {
            print_with_indent(stream, str, indent, indent);
        }

        /*!
         * \brief print description after specifiers in help message
         *
         * call this utility function after printing option specifiers
         *
         * \param stream output stream
         * \param description description string
         * \param specifiers_length length of specifiers in help message
         */
        inline void print_description(std::ostream& stream,
            const std::string& description, std::size_t specifiers_length) {
            if (indents::before_commands + specifiers_length +
                    indents::after_commands >
                indents::before_description) {
                impl::print_spaces(stream, indents::after_commands);
            } else {
                impl::print_spaces(stream,
                    indents::before_description -
                        (indents::before_commands + specifiers_length));
            }
            print_with_indent(
                stream, description, indents::before_description, 0);
            stream.put('\n');
        }

    }  // namespace impl
}  // namespace args
