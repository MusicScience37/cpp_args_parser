/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <functional>
#include <memory>
#include <string>

#include "combined_parser.h"
#include "parser_base.h"

namespace args {

    /*!
     * \brief class to parse sub commands
     */
    class sub_command : public parser_base {
    private:
        //! struct for data
        struct sub_command_data_struct {  // NOLINT
                                          // initialization is done by
                                          // sub_command class

            //! name of this sub command
            std::string name;

            //! callback function called when this sub command is used
            std::function<void()> callback;

            //! flag whether this sub command is used
            bool used;

            //! parser of options to this sub command
            combined_parser option_parser;

            //! abstract usage message
            std::string abstract_usage;
        };

        //! data
        std::shared_ptr<sub_command_data_struct> _data;

    public:
        /*!
         * \brief construct with the name of the sub command
         *
         * \param name name of the sub command
         */
        explicit sub_command(const std::string& name)
            : sub_command(name, [] {}) {}

        /*!
         * \brief construct with the name of the sub command and a flag
         *
         * \param name name of the sub command
         * \param used flag whether the sub command is used
         */
        sub_command(const std::string& name, bool& used)
            : sub_command(name, [&used] { used = true; }) {
            used = false;
        }

        /*!
         * \brief construct with the name of the sub command and callback
         *        function
         *
         * \param name name of the sub command
         * \param callback callback function called when the sub command is used
         */
        sub_command(
            const std::string& name, const std::function<void()>& callback)
            : _data(std::make_shared<sub_command_data_struct>()) {
            _data->name = name;
            _data->callback = callback;
            _data->used = false;
        }

        //! default copy constructor
        sub_command(const sub_command&) noexcept = default;

        //! default copy assignment operator
        sub_command& operator=(const sub_command&) noexcept = default;

        //! default move constructor
        sub_command(sub_command&&) noexcept = default;

        //! default move assignment operator
        sub_command& operator=(sub_command&&) noexcept = default;

        //! default destructor
        ~sub_command() override = default;

        /*!
         * \brief access to the parser of options of this sub command
         *
         * \return combined_parser& parser of options of this sub command
         */
        combined_parser& option_parser() const { return _data->option_parser; }

        /*!
         * \brief get whether this sub command is used
         *
         * \return bool whether this sub command is used
         */
        bool used() const { return _data->used; }

    protected:
        using parser_base::parse_impl;

        /*!
         * \brief parse an argument
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        std::size_t parse_impl(
            const args_holder& args, std::size_t ind) override {
            if (args[ind] != _data->name) {
                return 0;
            }
            _data->callback();
            _data->used = true;
            ++ind;
            std::size_t total_used = 1;
            while (ind < args.size()) {
                std::size_t used = parse_impl(_data->option_parser, args, ind);
                if (used == 0) {
                    throw parse_error("unexpected argument: " + args[ind]);
                }
                ind += used;
                total_used += used;
            }
            return total_used;
        }

    public:
        /*!
         * \brief set name of the executable
         *
         * This function change the name of the executable
         * of the inner parser for appropriate process to print help messages.
         *
         * \param str name of the executable
         */
        void exe_name(const std::string& str) override {
            parser_base::exe_name(str);
            _data->option_parser.exe_name(str + " " + _data->name);
        }

        using parser_base::exe_name;

        /*!
         * \brief set abstract usage message
         *
         * \param str abstract usage message
         */
        sub_command& abstract(const std::string& str) {
            _data->abstract_usage = str;
            return *this;
        }

        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        void print_option_description(std::ostream& stream) const override {
            stream << "Sub command '" << _data->name << "'\n";
            impl::print_spaces(stream, indents::before_commands);
            stream << exe_name() << " " << _data->name << " [options] "
                   << _data->option_parser.remaining_arguments_hints()
                   << "\n\n";
            if (!_data->abstract_usage.empty()) {
                impl::print_with_indent(
                    stream, _data->abstract_usage, indents::before_commands);
                stream.put('\n');
                stream.put('\n');
            }
            _data->option_parser.print_option_description(stream);
        }

        /*!
         * \brief get whether this parser is a sub command
         *
         * \return bool whether this parser is a sub command
         */
        bool is_sub_command() const override { return true; }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<sub_command>(*this);
        }
    };

}  // namespace args
