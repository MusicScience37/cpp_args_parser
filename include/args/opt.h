/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <functional>
#include <memory>
#include <stdexcept>
#include <string>

#include "parser_base.h"

namespace args {

    /*!
     * \brief class to parse options
     */
    class opt : public parser_base {
    private:
        //! struct for data
        struct opt_data_struct {  // NOLINT
                                  // initialization is done by opt class

            //! specifiers of this option
            std::vector<std::string> specifiers;

            //! description of this option
            std::string description;

            //! callback function to be called if this option is found
            std::function<void()> option_callback;

            //! flag whether this option is found at least once
            bool found;
        };

        //! data
        std::shared_ptr<opt_data_struct> _data;

    public:
        /*!
         * \brief construct an option with no callback
         */
        opt() : opt([] {}) {}

        /*!
         * \brief construct an option with a reference to the result
         *
         * \param found a reference to which this class writes
         *        whether the option is found
         */
        explicit opt(bool& found) : opt([&found] { found = true; }) {
            found = false;
        }

        /*!
         * \brief constrcut an option with a callback function
         *
         * \param callback callback function called when the option is found
         */
        explicit opt(const std::function<void()>& callback)
            : parser_base(), _data(std::make_shared<opt_data_struct>()) {
            _data->option_callback = callback;
            _data->found = false;
        }

        //! default copy constructor
        opt(const opt&) noexcept = default;

        //! default copy assignment operator
        opt& operator=(const opt&) noexcept = default;

        //! default move constructor
        opt(opt&&) noexcept = default;

        //! default move assignment operator
        opt& operator=(opt&&) noexcept = default;

        //! default destructor
        ~opt() override = default;

        /*!
         * \brief add a specifier to this option
         *
         * This function adds a specifier like `--name` (`name` is input string)
         * if input string is a string with its size longer than 2.
         * This function adds a specifier like `-a` (`a` is input string)
         * if input string is a single character.
         *
         * \param str string to make a specifier from
         * \return opt& this object
         */
        opt& specifier(const std::string& str) {
            if (str.empty()) {
                throw std::invalid_argument(
                    "input for args::opt::name must not be an empty string");
            }
            if (str.size() == 1) {
                return specifier(str[0]);
            }
            _data->specifiers.emplace_back("--" + str);
            return *this;
        }

        /*!
         * \brief add a specifier to this option
         *
         * This function adds a specifier like `-a` (`a` is input character).
         *
         * \param c character to make a specifier from
         * \return opt& this object
         */
        opt& specifier(char c) {
            _data->specifiers.emplace_back(std::string() + "-" + c);
            return *this;
        }

        //! \copydoc specifier(const std::string& str)
        opt& operator[](const std::string& str) { return specifier(str); }

        //! \copydoc specifier(char c)
        opt& operator[](char c) { return specifier(c); }

        /*!
         * \brief set description of this option
         *
         * This class adds indents after each line feed characters.
         *
         * \param str description string
         * \return opt& this object
         */
        opt& description(const std::string& str) {
            _data->description = str;
            return *this;
        }

        //! \copydoc description(const std::string& str)
        opt& operator()(const std::string& str) { return description(str); }

        /*!
         * \brief get whether this option is found
         *
         * \return bool whether this option is found
         */
        bool found() const { return _data->found; }

    protected:
        /*!
         * \brief parse an argument
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        std::size_t parse_impl(
            const args_holder& args, std::size_t ind) override {
            for (const std::string& specifier : _data->specifiers) {
                if (args[ind] == specifier) {
                    _data->found = true;
                    _data->option_callback();
                    return 1;
                }
            }
            return 0;
        }

    public:
        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        void print_option_description(std::ostream& stream) const override {
            impl::print_spaces(stream, indents::before_commands);
            std::size_t commands_length = 0;

            for (std::size_t i = 0; i < _data->specifiers.size() - 1; ++i) {
                stream << _data->specifiers[i] << ", ";
                commands_length += _data->specifiers[i].size() + 2;
            }
            {
                const std::size_t i = _data->specifiers.size() - 1;
                stream << _data->specifiers[i];
                commands_length += _data->specifiers[i].size();
            }

            impl::print_description(
                stream, _data->description, commands_length);
        }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<opt>(*this);
        }
    };

}  // namespace args
