/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <string>
#include <vector>

namespace args {

    /*!
     * \brief class to hold arguments to be processed
     */
    class args_holder {
    private:
        //! arguments
        std::vector<std::string> _args;

    public:
        /*!
         * \brief construct from arguments in a vector
         *
         * \param args vector of arguments
         */
        explicit args_holder(const std::vector<std::string>& args)
            : _args(args) {}

        /*!
         * \brief constrcut from arguments as in main function
         *
         * \param argc number of arguments
         * \param argv array of arguments
         */
        explicit args_holder(
            int argc, char* argv[])  // NOLINT
                                     // C-style array is needed here to receive
                                     // argv in main function
            : _args() {
            _args.reserve(static_cast<std::size_t>(argc));
            for (int i = 0; i < argc; ++i) {
                _args.emplace_back(argv[i]);
            }
        }

        //! default copy constructor
        args_holder(const args_holder&) = default;

        //! default copy assignment operator
        args_holder& operator=(const args_holder&) = default;

        //! default move constructor
        args_holder(args_holder&&) = default;

        //! default move assignment operator
        args_holder& operator=(args_holder&&) = default;

        //! default destructor
        ~args_holder() = default;

        /*!
         * \brief get the i-th argument
         *
         * \param i index of the argument
         * \return const std::string& i-th argument
         */
        const std::string& operator[](std::size_t i) const { return _args[i]; }

        /*!
         * \brief get the number of arguments
         *
         * \return std::size_t the number of arguments
         */
        std::size_t size() const { return _args.size(); }
    };

}  // namespace args