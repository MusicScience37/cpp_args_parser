/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <memory>

#include "parser_base.h"

namespace args {

    /*!
     * \brief class of parsers made of multiple parsers
     */
    class combined_parser : public parser_base {
    private:
        //! struct for data
        struct combined_parser_data_struct {
            //! parsers
            std::vector<std::shared_ptr<parser_base> > parsers;
        };

        //! data
        std::shared_ptr<combined_parser_data_struct> _data;

    public:
        //! default constructor
        combined_parser()
            : parser_base(),
              _data(std::make_shared<combined_parser_data_struct>()) {
            // when this class is used, at least 2 parsers will be added
            _data->parsers.reserve(2);
        }

        //! default copy constructor
        combined_parser(const combined_parser&) noexcept = default;

        //! default copy assignment operator
        combined_parser& operator=(const combined_parser&) noexcept = default;

        //! default move constructor
        combined_parser(combined_parser&&) noexcept = default;

        //! default move assignment operator
        combined_parser& operator=(combined_parser&&) noexcept = default;

        //! default destructor
        ~combined_parser() override = default;

        /*!
         * \brief add a parser
         *
         * \param parser parser
         * \return combined_parser& this object
         */
        combined_parser& push_back(const parser_base& parser) {
            _data->parsers.push_back(parser.clone());
            return *this;
        }

        /*!
         * \brief add parsers in another combined_parser object
         *
         * \param parser combined_parser object
         * \return combined_parser& this object
         */
        combined_parser& push_back(const combined_parser& parser) {
            _data->parsers.insert(_data->parsers.end(),
                parser._data->parsers.begin(), parser._data->parsers.end());
            return *this;
        }

        //! \copydoc push_back(const parser_base& parser)
        combined_parser& operator|=(const parser_base& parser) {
            return push_back(parser);
        }

        //! \copydoc push_back(const combined_parser& parser)
        combined_parser& operator|=(const combined_parser& parser) {
            return push_back(parser);
        }

        /*!
         * \brief constrcut and add a parser
         *
         * This function is meant for use in implicit convertions.
         *
         * \param parser parser
         */
        combined_parser(const parser_base& parser)  // NOLINT
            : combined_parser() {
            push_back(parser);
        }

        /*!
         * \brief change this object with a parser
         *
         * This function is meant for use in implicit convertions.
         *
         * \param parser parser
         * \return combined_parser& this object
         */
        combined_parser& operator=(const parser_base& parser) {
            return operator=(combined_parser(parser));
        }

    protected:
        using parser_base::parse_impl;

        /*!
         * \brief parse an argument
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        std::size_t parse_impl(
            const args_holder& args, std::size_t ind) override {
            for (auto& parser_ptr : _data->parsers) {
                parser_base& parser = *parser_ptr;
                std::size_t used = parse_impl(parser, args, ind);
                if (used > 0) {
                    return used;
                }
            }
            return 0;
        }

    public:
        /*!
         * \brief set name of the executable
         *
         * This function set the name of the executable
         * to the parsers too.
         *
         * \param str name of the executable
         */
        void exe_name(const std::string& str) override {
            parser_base::exe_name(str);
            for (auto& ptr : _data->parsers) {
                ptr->exe_name(str);
            }
        }

        using parser_base::exe_name;

        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        void print_option_description(std::ostream& stream) const override {
            for (auto& parser_ptr : _data->parsers) {
                if (parser_ptr->is_sub_command()) {
                    continue;
                }
                parser_ptr->print_option_description(stream);
            }
            for (auto& parser_ptr : _data->parsers) {
                if (!parser_ptr->is_sub_command()) {
                    continue;
                }
                stream << "\n";
                parser_ptr->print_option_description(stream);
            }
        }

        /*!
         * \brief get hint string of remaining arguments to print in usage
         *        messages
         *
         * \return std::string hint string
         */
        std::string remaining_arguments_hints() const override {
            std::string total_hint;
            for (auto& parser_ptr : _data->parsers) {
                std::string hint = parser_ptr->remaining_arguments_hints();
                if (!hint.empty()) {
                    if (!total_hint.empty()) {
                        total_hint.push_back(' ');
                    }
                    total_hint += hint;
                }
            }
            return total_hint;
        }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        std::shared_ptr<parser_base> clone() const override {
            return std::make_shared<combined_parser>(*this);
        }
    };

    /*!
     * \brief combine two parsers
     *
     * \param left parser
     * \param right parser
     * \return combined_parser combined parser
     */
    inline combined_parser operator|(
        const parser_base& left, const parser_base& right) {
        return combined_parser(left) |= right;
    }

    /*!
     * \brief combine another parser
     *
     * \param left parser
     * \param right parser
     * \return combined_parser combined parser
     */
    inline combined_parser operator|(
        const combined_parser& left, const parser_base& right) {
        return combined_parser(left) |= right;
    }

    /*!
     * \brief combine another parser
     *
     * \param left parser
     * \param right parser
     * \return combined_parser combined parser
     */
    inline combined_parser operator|(
        const parser_base& left, const combined_parser& right) {
        return combined_parser(left) |= right;
    }

    /*!
     * \brief combine another parser
     *
     * \param left parser
     * \param right parser
     * \return combined_parser combined parser
     */
    inline combined_parser operator|(
        const combined_parser& left, const combined_parser& right) {
        return combined_parser(left) |= right;
    }

}  // namespace args
