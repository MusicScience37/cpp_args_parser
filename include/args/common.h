/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <cstddef>
#include <stdexcept>

namespace args {

    /*!
     * \brief class of exception to express parse errors
     */
    class parse_error : public std::runtime_error {
    public:
        using std::runtime_error::runtime_error;
    };

    //! number of indents in help messages
    struct indents {
        enum constants : std::size_t {
            //! number of spaces before commands
            before_commands = 2,
            //! minimum number of spaces after commands
            after_commands = 4,
            /*!
             * \brief number of spaces before commands
             *        (count from the beggining of lines)
             */
            before_description = 30
        };
    };

}  // namespace args
