/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

#include "args_holder.h"
#include "common.h"
#include "help_util.h"

namespace args {

    /*!
     * \brief base class of parsers
     */
    class parser_base {
    private:
        //! struct of data
        struct base_data_struct {
            //! error string
            std::string error_str;

            //! name of executable
            std::string exe_name;

            //! abstract usage message
            std::string abstract;
        };

        //! data
        std::shared_ptr<base_data_struct> _base_data;

    protected:
        //! default constructor
        parser_base() : _base_data(std::make_shared<base_data_struct>()) {
            _base_data->exe_name = "<path to me>";
        }

        //! default copy constructor
        parser_base(const parser_base&) noexcept = default;

        //! default copy assignment operator
        parser_base& operator=(const parser_base&) noexcept = default;

        //! default move constructor
        parser_base(parser_base&&) noexcept = default;

        //! default move assignment operator
        parser_base& operator=(parser_base&&) noexcept = default;

    public:
        //! default destructor
        virtual ~parser_base() = default;

    protected:
        /*!
         * \brief parse an argument
         *
         * This function must be implemented in derived classes.
         * Implementations in derived classes are assumed to
         * parse arguments from ind-th one
         * and return the number of arguments used in the derived class
         * (zero if no argument was used).
         * If an error occurred, throw an exception.
         *
         * \warning This function is meant for internal use.
         *          User codes should use parse function instead.
         *
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        virtual std::size_t parse_impl(
            const args_holder& args, std::size_t ind) = 0;

        /*!
         * \brief call parse_impl(const args_holder& args, std::size_t ind)
         *        from derived classes
         *
         * \param parser parser
         * \param args arguments
         * \param ind index of the argument to be processed from
         * \return std::size_t number of arguments used in this function
         */
        static std::size_t parse_impl(
            parser_base& parser, const args_holder& args, std::size_t ind) {
            return parser.parse_impl(args, ind);
        }

    public:
        /*!
         * \brief parse arguments
         *
         * \param args arguments
         * \return int return code (EXIT_SUCCESS or EXIT_FAILURE)
         */
        int parse(const args_holder& args) {
            try {
                std::size_t ind = 1;
                while (ind < args.size()) {
                    std::size_t used = parse_impl(args, ind);
                    if (used == 0) {
                        _base_data->error_str =
                            "unexpected argument: " + args[ind];
                        return EXIT_FAILURE;
                    }
                    ind += used;
                }
                _base_data->error_str = "";
                return EXIT_SUCCESS;
            } catch (parse_error& e) {
                _base_data->error_str = e.what();
                return EXIT_FAILURE;
            } catch (std::exception& e) {
                _base_data->error_str =
                    std::string() + "error in parsing process: " + e.what();
                return EXIT_FAILURE;
            }
        }

        /*!
         * \brief parse arguments
         *
         * \param args vector of arguments
         * \return int return code (EXIT_SUCCESS or EXIT_FAILURE)
         */
        int parse(const std::vector<std::string>& args) {
            return parse(args_holder(args));
        }

        /*!
         * \brief parse arguments
         *
         * Interface for use in main function
         *
         * \param argc number of arguments
         * \param argv array of arguments
         * \return int return code (EXIT_SUCCESS or EXIT_FAILURE)
         */
        int parse(int argc, char* argv[])  // NOLINT
                                           // C-style array is needed here to
                                           // receive argv in main function
        {
            return parse(args_holder(argc, argv));
        }

        /*!
         * \brief get error string
         *
         * \return const std::string& error string
         */
        const std::string& error_str() const { return _base_data->error_str; }

        /*!
         * \brief set name of the executable
         *
         * This function doesn't return a reference to this object,
         * because this class can't get a reference to the derived class.
         *
         * Derived classes can modify the behavior of this function.
         *
         * \warning This function should be called after constructing and
         *          combining parsers.
         *
         * \param str name of the executable
         */
        virtual void exe_name(const std::string& str) {
            _base_data->exe_name = str;
        }

        /*!
         * \brief get name of the executable
         *
         * \return const std::string& name of the executable
         */
        const std::string& exe_name() const { return _base_data->exe_name; }

        /*!
         * \brief set abstract usage message
         *
         * This function doesn't return a reference to this object,
         * because this class can't get a reference to the derived class.
         *
         * \param str abstract usage message
         */
        void abstract(const std::string& str) { _base_data->abstract = str; }

        /*!
         * \brief print lines of the description of this option
         *
         * Use print_help function to print the whole help message.
         *
         * \param stream stream to ouput to
         */
        virtual void print_option_description(std::ostream& stream) const = 0;

        /*!
         * \brief get hint string of remaining arguments to print in usage
         *        messages
         *
         * Derived classes must override this function
         * if they parses some arguments other than options.
         *
         * \return std::string hint string
         */
        virtual std::string remaining_arguments_hints() const { return ""; }

        /*!
         * \brief print help message
         *
         * \param stream stream to output to
         */
        void print_help(std::ostream& stream) const {
            stream << "Usage\n";
            impl::print_spaces(stream, indents::before_commands);
            stream << _base_data->exe_name << " [options] "
                   << remaining_arguments_hints() << "\n\n";
            if (!_base_data->abstract.empty()) {
                impl::print_with_indent(
                    stream, _base_data->abstract, indents::before_commands);
                stream.put('\n');
                stream.put('\n');
            }
            print_option_description(stream);
            stream.flush();
        }

        /*!
         * \brief get whether this parser is a sub command
         *
         * \return bool whether this parser is a sub command
         */
        virtual bool is_sub_command() const { return false; }

        /*!
         * \brief make a copy of this object
         *
         * \return std::shared_ptr<parser_base> pointer to the copy
         */
        virtual std::shared_ptr<parser_base> clone() const = 0;
    };

    inline std::ostream& operator<<(
        std::ostream& stream, const parser_base& parser) {
        parser.print_help(stream);
        return stream;
    }

}  // namespace args
