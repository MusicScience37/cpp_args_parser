/*
Copyright 2019 MusicScience37 (Kenta Kabashima)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
#pragma once

// clang-format off
#include "args_holder.h"
#include "common.h"
#include "help_util.h"
#include "stream_parser.h"
#include "parser_base.h"
#include "opt.h"
#include "arg.h"
#include "remaining.h"
#include "combined_parser.h"
#include "sub_command.h"
// clang-format on
