.. CppArgsParser documentation master file, created by
   sphinx-quickstart on Sun Dec 29 21:43:03 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CppArgsParser
=====================

parser of arguments of programs in C++

Index
----------------

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    overview
    apis/index

* :ref:`search`
